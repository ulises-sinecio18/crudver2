<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\facades\DB;
use App\Empleado;

class CrudController extends Controller
{
  
    public function index()
    {
        $empleados = DB::select('select * from empleados');
        return view('inicio')->with('empleados', $empleados);
    }

    
    public function create()
    {
        return view('crear-empleado');
    }

   
    public function store(Request $request)
    {
        $datos = request()->validate([
            'nombre'=> ['required', 'string', 'max:20'],
            'apellidop'=> ['required', 'string', 'max:20'],
            'apellidom'=> ['required', 'string', 'max:20'],
            'telefono'=> ['required', 'string', 'max:20'],
            'salario'=> ['required', 'string', 'max:20'],
        ]);

        DB::table('empleados')->insert(['nombre' => $datos["nombre"], 'apellidoP' => $datos["apellidop"], 'apellidoM' => $datos["apellidom"], 'telefono' => $datos["telefono"], 'salario' => $datos["salario"]]);

        return redirect('/');
    }

   
    public function show($id)
    {
       $empleado = Empleado::find($id);
       return view('empleado')->with('empleado', $empleado);
    }

 
    public function edit($id)
    {
        $empleado = Empleado::find($id);
       return view('editar-empleado')->with('empleado', $empleado);

       
    }

    
    public function update(Request $request, $id)
    {
        $datos = request()->validate([
            'nombre'=> ['required', 'string', 'max:20'],
            'apellidop'=> ['required', 'string', 'max:20'],
            'apellidom'=> ['required', 'string', 'max:20'],
            'telefono'=> ['required', 'string', 'max:15'],
            'salario'=> ['required', 'string', 'max:15'],
        ]);

        DB::table('empleados')->where('id', $id)->update(['nombre' => $datos["nombre"], 'apellidoP' => $datos["apellidop"], 'apellidoM' => $datos["apellidom"], 'telefono' => $datos["telefono"], 'salario' => $datos["salario"]]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('empleados')->where('id', $id)->delete();
        return redirect('/');
    }
}
