<?php

use Illuminate\Support\Facades\Route;


//Route::get('/ejemplo/{id}', "CrudController@index");

//Route::resource('ejemplo', 'CrudController');

//Route::get('/contacto', 'CrudController@index');

Route::get('/', 'CrudController@index');
Route::get('crear-empleado', 'CrudController@create');
Route::post('crear-empleado', 'CrudController@store');
Route::get('show/{id}', 'CrudController@show');
Route::get('edit/{id}', 'CrudController@edit');
Route::put('update/{id}', 'CrudController@update');
ROUTE::get('delete/{id}', 'CrudController@destroy');