<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<a href="{{url('/')}}">
		
		<button> Volver a Inicio</button>

	</a>
	<br><br>
<center>
	<h1>Editar el empleado</h1>

	<form method="post" action="{{url('update/' .$empleado -> id) }}" novalidate>

		@csrf

		@method('put')

		<h2>Hombre:</h2>
		<input type="text" name="nombre" maxlength="10" required autocomplete="off" value="{{$empleado -> nombre}}" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))" auto />
	
		@error('nombre')

		<p>Colocar bien el nombre</p>

		@enderror

		<h2>Apellido Paterno</h2>
		<input type="text" name="apellidop" maxlength="15" required autocomplete="off" value="{{$empleado -> apellidoP}}" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))" auto />
		
		@error('apellidop')

		<p>Colocar bien el apellido</p>

		@enderror

		<h2>Apellido Materno</h2>
		<input type="text" name="apellidom" maxlength="15" required autocomplete="off" value="{{$empleado -> apellidoM}}" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))" auto /> 
		@error('apellidom')

		<p>Colocar bien el apellido</p>

		@enderror

		<h2>Teléfono</h2>
		<input type="text" name="telefono" maxlength="10" required autocomplete="off" value="{{$empleado -> telefono}}" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" auto />
		
		@error('telefono')

		<p>Colocar bien el telefono</p>

		@enderror

		<h2>Salario</h2>
		<input type="text" name="salario" maxlength="10" required autocomplete="off" value="{{$empleado -> salario}}" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" auto />
		
		@error('salario')

		<p>Colocar bien el salario</p>

		@enderror

		<br><br>
		<button type='submit'>Guardar</button>
	</form>
</center>

</body>
</html>