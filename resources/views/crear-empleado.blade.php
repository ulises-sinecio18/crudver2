<!DOCTYPE html>
<html>
<head>
	<title>CRUD</title>
</head>
<body>

	<a href="{{url('/')}}">
		
		<button> Volver a Inicio</button>

	</a>
<center>
<H1>Crear Empleado</H1>

<form method="post" novalidate="">
	@csrf

	<h2>Nombre(s)</h2>
	<input type="text" name="nombre" maxlength="20" required autocomplete="off" placeholder="Ingresar nombres" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode ==32))" auto />
	@error('nombre')

	<p>Colocar bien el nombre</p>

	@enderror

	<h2>Apellido Paterno</h2>
	<input type="text" name="apellidop" maxlength="10" required autocomplete="off" placeholder="Ingresar apellidos paterno" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))" auto />
	@error('apellidop')

	<p>Colocar bien el apellido</p>

	@enderror

	<h2>Apellido Materno</h2>
	<input type="text" name="apellidom" maxlength="10" required autocomplete="off" placeholder="Ingresar apellido materno" onkeypress="return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122))" auto /> 
	@error('apellidom')

	<p>Colocar bien el apellido</p>

	@enderror

	<h2>Teléfono</h2>
	<input type="text" name="telefono" maxlength="11" required autocomplete="off" placeholder="Ingesar número telefonico" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" auto />
	@error('telefono')

	<p>Colocar bien el telefono</p>

	@enderror

	<h2>Salario</h2>
	<input type="text" name="salario" maxlength="10" required autocomplete="off" placeholder="Ingesar salario" onkeypress="return ((event.charCode >= 48 && event.charCode <= 57) || (event.charCode ==46))" auto />
	@error('salario')

	<p>Colocar bien el salario</p>

	@enderror

	<br><br>
	<button type='submit'>Crear</button>

</form>	
</center>

</body>
</html>

