<!DOCTYPE html>
<html>
<head>
	
	<title>CRUD</title>
</head>
<body>

	<a href="{{url('crear-empleado')}}">
		
		<button> Crear Empleado</button>

	</a>

	<br><br>

	<table border="1">
		
		<thead>
			
			<tr>
				<th>Nombre(s)</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>Telefono</th>
				<th>Salario</th>
				<th>Accciones</th>
				<th>Borrar</th>
			</tr>

		</thead>
		<tbody>
			
			@foreach( $empleados as $empleado)

				<tr>
					
					<td>{{$empleado -> nombre}}</td>
					<td>{{$empleado -> apellidoP}}</td>
					<td>{{$empleado -> apellidoM}}</td>
					<td>{{$empleado -> telefono}}</td>
					<td>{{$empleado -> salario}}</td>

					<td>
						<a href="{{url('show/'.$empleado ->id) }}">		
							<button>Ver</button>
						</a>
						<a href="{{url('edit/'.$empleado ->id) }}">						
							<button>Editar</button>
						</a>
					</td>
					<td>
						<form method="get" action="{{url('delete/' .$empleado -> id) }}">
						
							@csrf



							<button type="submit">Eliminar</button>
						</form>
					</td>
				</tr>

			@endforeach

		</tbody>

	</table>
</body>
</html>